﻿using AttendanceModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttendanceModule.Core
{
    public static class AuthorizeCache
    {
        private static Dictionary<string, Account> cache
            = new Dictionary<string, Account>();

        public static void CreateCache(string sessionId)
        {
            if (!cache.ContainsKey(sessionId))
            {
                cache.Add(sessionId, null);
            }
        }

        public static void ClearCache(string sessionId)
        {
            if (!cache.ContainsKey(sessionId))
            {
                cache.Remove(sessionId);
            }
        }
    }
}