﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttendanceModule.Models
{
    public class Account
    {
        public string Username { get; set; }
        public string Email { get; set; }
    }
}